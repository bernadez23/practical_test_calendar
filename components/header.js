Vue.component('top-header', {
    template: `
        <div class="top-header">
            <div class="container-fluid">
                <div class="row no-gutters">

                    <!-- milestone and calendar view -->
                    <div class="col-sm-12 col-lg-3">
                        <div class="row no-gutters">
                            <div class="col-xs-4">
                                <div class="miles-num">
                                    1
                                    <span class="miles-badge">
                                        2
                                    </span>
                                </div>
                                <p>Milestone 1</p>
                            </div>

                            <div class="col-xs-8">
                                <ul class="calendar-view">
                                    <li>
                                        <a href="#" class="active">
                                            <svg  viewBox="0 0 25 22" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <rect x="1" y="3" width="23" height="18" stroke="#43B3E9"
                                                    stroke-width="2" />
                                                <line x1="6" x2="6" y2="3" stroke="#43B3E9" stroke-width="2" />
                                                <line x1="19" x2="19" y2="3" stroke="#43B3E9" stroke-width="2" />
                                                <rect x="11" y="7" width="2" height="2" fill="#43B3E9" />
                                                <rect x="15" y="7" width="2" height="2" fill="#43B3E9" />
                                                <rect x="19" y="7" width="2" height="2" fill="#43B3E9" />
                                                <rect x="19" y="11" width="2" height="2" fill="#43B3E9" />
                                                <rect x="15" y="11" width="2" height="2" fill="#43B3E9" />
                                                <rect x="11" y="11" width="2" height="2" fill="#43B3E9" />
                                                <rect x="8" y="11" width="2" height="2" fill="#43B3E9" />
                                                <rect x="4" y="11" width="2" height="2" fill="#43B3E9" />
                                                <rect x="4" y="15" width="2" height="2" fill="#43B3E9" />
                                                <rect x="8" y="15" width="2" height="2" fill="#43B3E9" />
                                                <rect x="11" y="15" width="2" height="2" fill="#43B3E9" />
                                                <rect x="15" y="15" width="2" height="2" fill="#43B3E9" />
                                            </svg>
                                            <small>Month</small>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#">
                                            <svg viewBox="0 0 25 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect x="1" y="3" width="23" height="18" stroke="#939393" stroke-width="2"/>
                                                <line x1="6" x2="6" y2="3" stroke="#939393" stroke-width="2"/>
                                                <line x1="19" x2="19" y2="3" stroke="#939393" stroke-width="2"/>
                                                <rect x="6" y="11" width="2" height="2" fill="#857F7F"/>
                                                <rect x="9" y="11" width="2" height="2" fill="#857F7F"/>
                                                <rect x="12" y="11" width="2" height="2" fill="#857F7F"/>
                                                <rect x="15" y="11" width="2" height="2" fill="#857F7F"/>
                                                <rect x="18" y="11" width="2" height="2" fill="#857F7F"/>
                                                <rect x="5" y="8" width="16" height="2" fill="#C4C4C4"/>
                                                <rect x="5" y="14" width="16" height="2" fill="#C4C4C4"/>
                                            </svg>
                                            <small>Week</small>
                                        </a>
                                    </li>

                                    <li>
                                    <a href="#">
                                            <svg viewBox="0 0 25 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect x="1" y="3" width="23" height="18" stroke="#939393" stroke-width="2"/>
                                                <line x1="6" x2="6" y2="3" stroke="#939393" stroke-width="2"/>
                                                <line x1="19" x2="19" y2="3" stroke="#939393" stroke-width="2"/>
                                                <circle cx="13" cy="12" r="6" fill="#AAA4A4"/>
                                                <path d="M12.1108 11.4023H12.938C13.3319 11.4023 13.6237 11.3039 13.8135 11.1069C14.0033 10.91 14.0981 10.6486 14.0981 10.3228C14.0981 10.0076 14.0033 9.76237 13.8135 9.58691C13.6273 9.41146 13.3695 9.32373 13.04 9.32373C12.7428 9.32373 12.494 9.40609 12.2935 9.5708C12.0929 9.73193 11.9927 9.9432 11.9927 10.2046H10.4404C10.4404 9.79639 10.5496 9.43115 10.7681 9.10889C10.9901 8.78304 11.298 8.52881 11.6919 8.34619C12.0894 8.16357 12.5262 8.07227 13.0024 8.07227C13.8296 8.07227 14.4777 8.271 14.9468 8.66846C15.4159 9.06234 15.6504 9.60661 15.6504 10.3013C15.6504 10.6593 15.5412 10.9888 15.3228 11.2896C15.1043 11.5903 14.8179 11.8213 14.4634 11.9824C14.9038 12.14 15.2314 12.3763 15.4463 12.6914C15.6647 13.0065 15.7739 13.3789 15.7739 13.8086C15.7739 14.5033 15.5197 15.0601 15.0112 15.479C14.5063 15.8979 13.8368 16.1074 13.0024 16.1074C12.2218 16.1074 11.5827 15.9015 11.085 15.4897C10.5908 15.078 10.3438 14.5337 10.3438 13.8569H11.896C11.896 14.1506 12.0052 14.3905 12.2236 14.5767C12.4456 14.7629 12.7178 14.856 13.04 14.856C13.4089 14.856 13.6971 14.7593 13.9048 14.5659C14.116 14.369 14.2217 14.1094 14.2217 13.7871C14.2217 13.0065 13.792 12.6162 12.9326 12.6162H12.1108V11.4023Z" fill="#ECDCDC"/>
                                            </svg>
                                            <small>Day</small>
                                    </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- month carousel -->

                <div class="col-sm-12 col-lg-9">
                        <div class="calendar-months">
                            <a href="#" @click.stop="prevMonth($event)" class="calendar-control c-left"><i class="fas fa-angle-left"></i></a>
                            <a href="#" @click.stop="nextMonth($event)"  class="calendar-control c-right"><i class="fas fa-angle-right"></i></a>

                            <div class="calendar-month-wrapper">
                                <ul class="calendar-monts-container">
                                    <li ><span>JAN</span></li>
                                    <li><span>FEB</span></li>
                                    <li><span>MAR</span></li>
                                    <li><span>APR</span></li>
                                    <li class="calendar-monts-active"><span>MAY</span></li>
                                    <li><span>JUN</span></li>
                                    <li><span>JUL</span></li>
                                    <li><span>AUG</span></li>
                                    <li><span>SEP</span></li>
                                    <li><span>OCT</span></li>
                                    <li><span>NOV</span></li>
                                    <li><span>DEC</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
    methods : {
        moveSlide(currentMonth, monthSlideTo) {
            let monthsContainer = this.$el.querySelector('.calendar-monts-container');
            let moveSize = monthSlideTo.style.left;

            // move the current month to the prev
            monthsContainer.style.transform = 'translateX(-'+moveSize+')';

            // don't remove class if current and monthslideto left value is equal
            if(currentMonth.style.left != moveSize) {
                // add class to prev month
                monthSlideTo.classList.add('calendar-monts-active');
                // remove class from current month
                currentMonth.classList.remove('calendar-monts-active');
            }
        },
        nextMonth(event) {
            let currentMonth = this.$el.querySelector('.calendar-monts-active');
            let nextMonth = currentMonth.nextElementSibling;

            // move the current month to the next
            this.moveSlide(currentMonth, nextMonth);

        },
        prevMonth(event) {
            let currentMonth = this.$el.querySelector('.calendar-monts-active');
            let prevMonth = currentMonth.previousElementSibling;

            //move to previous month
            this.moveSlide(currentMonth, prevMonth);
        },
        arrangeMonths() {
            // arrange months position
            let monthsContainer = this.$el.querySelector('.calendar-monts-container');
            let months = Array.from(monthsContainer.children);

            months.forEach((month, index) => {
                month.style.left = 100 * index + 'px';
            });

            // set the current position of the month
            let currentMonth = this.$el.querySelector('.calendar-monts-active');
            this.moveSlide(currentMonth, currentMonth)
        }
    },
    mounted() {
        // run arrangeMonths positin 
        this.arrangeMonths();
    }
});