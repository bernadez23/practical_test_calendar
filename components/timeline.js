Vue.component('timeline', {
    template : `
        <div class="time-schedule">
            <ul>
                <li>8 AM</li>
                <li>9 AM</li>
                <li>10 AM 
                    <span class="scheduled sc-1">
                        <p><i class="fas fa-user"></i> MEETING</p>
                        <p><i class="fas fa-map-marker-alt"></i> Business Name</p>
                    </span>
                </li>
                <li>11 AM</li>
                <li>12 PM</li>
                <li>1 PM
                    <span class="scheduled sc-2">
                        <p><i class="fas fa-user"></i> DEMO</p>
                        <p><i class="fas fa-map-marker-alt"></i> UVU</p>
                    </span>
                </li>
                <li>2 PM
                    <span class="scheduled sc-3">
                        <p><i class="fas fa-user"></i> DEMO</p>
                        <p><i class="fas fa-map-marker-alt"></i> UVU</p>
                    </span>
                </li>
                <li>3 PM
                    <span class="scheduled sc-2">
                        <p><i class="fas fa-user"></i> DEMO</p>
                        <p><i class="fas fa-map-marker-alt"></i> UVU</p>
                    </span>

                </li>
                <li>4 PM
                    <span class="scheduled sc-3">
                        <p><i class="fas fa-user"></i> DEMO</p>
                        <p><i class="fas fa-map-marker-alt"></i> UVU</p>
                    </span>
                </li>
                <li>5 PM</li>
                <li>6 PM</li>
                <li>7 PM</li>
                <li>8 PM</li>
            </ul>
        </div>
    `
})