Vue.component('chart', {
    template : `
        <div class="chart-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="label">
                            <h3>TOTAL SALES</h3>
                            <ul>
                                <li><span class="box green"></span> CURRENT MONTH</li>
                                <li><span class="box gray"></span> LAST MONTH</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="chart sales-chart">

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="label">
                            <h3>TOTAL HOURS</h3>
                            <ul>
                                <li><span class="box green"></span> COMPLETE</li>
                                <li><span class="box red"></span> PAST DUE</li>
                                <li><span class="box blue"></span> COMING UP</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6 order-lg-first">
                        <div class="chart hours-chart">

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="label">
                            <h3>TOTAL TASK</h3>
                            <ul>
                                <li><span class="box green"></span> COMPLETE</li>
                                <li><span class="box red"></span> PAST DUE</li>
                                <li><span class="box gray"></span> COMING UP</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="chart task-chart">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
    data() {
        return {
            hours : {
                service : [12, 88],
                call : [30, 70],
                demo : [50, 50]
            }
        }
    },
    methods : {
        peiChart(options) {
            // options properties
            // {
            //      element : '',
            //      data : '',
            //      color  : [],
            //      doughnut : '' default 0;
            // }
            // styling
            
            // defaults 
            options.doughnut = typeof options.doughnut == "undefined" ? 0 : options.doughnut;
            options.multiple = typeof options.multiple == "undefined" ? false : options.multiple;
            
            let margin = {
                top : 20,
                right : 20,
                bottom : 20,
                left : 20
            }

            let width = 200 - margin.right - margin.left;

            let height = 200 - margin.top - margin.bottom;

            let radius = width/2;

            // color
            let color = d3.scale.ordinal()
                .range(options.color);
            
            // pie generator
            let pie = d3.layout.pie()
                .sort(null)
                .value(function(d) {return d.count;});

            let doughnut = radius - options.doughnut;

            if(options.doughnut == 0) {
                doughnut = 0;
            }

            //arc generator
            let arc = d3.svg.arc()
                .innerRadius(doughnut)
                .outerRadius(radius - 10);

            // define the chart
            let svg = d3.select(options.element).append("svg")
                .attr("width", width)
                .attr("height", height)
                .append("g")
                .attr("transform", "translate(" + width/2 + "," + height/2 + ")");
            
            // import datas
            d3.csv(options.data, function(err, data){
                if(err) throw err;
                
                // parse the data
                data.forEach(function(d) {
                    d.count = +d.count;
                    d.name = d.name;
                });

                // add elements
                let g = svg.selectAll(".arc")
                    .data(pie(data))
                    .enter().append("g")
                    .attr("class", "arc");

                // append path
                g.append("path")
                    .attr("d", arc)
                    .style("fill", function(d) { return color(d.data.name); })
                    .append("svg:title")
                    .text(function(d, i) { 
                        return data[i].count
                    });
                
            });

        },
        multiPie() {
            let width = 150,
                height = 150,
                cwidth = 20;
            
            let color = d3.scale.ordinal()
                .range(['#81CD4E', '#DADCDE', '#43B3E9']);
            
            let pie = d3.layout.pie()
                .sort(null);
            
            let arc = d3.svg.arc();
            
            let svg = d3.select(".hours-chart").append("svg")
                .attr("width", width)
                .attr("height", height)
                .append("g")
                .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
            
            let gs = svg.selectAll("g")
                .data(d3.values(this.hours))
                .enter()
                .append("g")
                .attr('class', function(d, i) {
                    return 'arc'+i;
                });
            
            gs.selectAll("path")
                .data(function(d) { return pie(d); })
                .enter()
                .append("path")
                .style("fill", function(d, i, f) { 
                    return color(i);
                })
                .attr("d", function(d, i, j) { return arc.innerRadius(15+cwidth*j).outerRadius(cwidth*(j+1))(d); });
            
            // sum all total hours
            let totalHours = this.hours.call[0] + this.hours.demo[0] + this.hours.service[0];
            
        }
    },
    mounted() {
        // peichart for sales
        this.peiChart({
            element : '.sales-chart',
            color : ['#81CD4E', '#DADCDE'],
            data : './data/sales.csv'
        });

        // peichart for task
        this.peiChart({
            element : '.task-chart',
            color : ['#81CD4E', '#DADCDE', '#EC3135'],
            data : './data/task.csv',
            doughnut : 30
        });

        // hours pie
        this.multiPie()
        
    }
});