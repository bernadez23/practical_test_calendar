Vue.component('calendar', {
    template : `
    <!-- calendar body -->
    <div class="calendar-body container-fluid">
        <div class="bottom-header">
            <ul class="day">
                <template v-for="(day, index) in days">
                    <li :key="index">{{day}}</li>
                </template>
            </ul>
        </div>

        <div class="row">
            
            
            <div class="col-sm-12 col-lg-6">
                <div class="main-calendar">
                    <a href="#" class="calendtar-control-view m-up">
                            <i class="fas fa-angle-up"></i>
                        </a>
                    <table class="table">
                        <tbody>
                            <template v-for="(daterow, index) in dates"> 
                                <tr :key="index">
                                    <template v-for="(date, dateindex) in daterow">
                                        <td class="d-date" :key="dateindex">{{date}}</td>
                                    </template>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                    <a href="#" class="calendtar-control-view m-down">
                            <i class="fas fa-angle-down"></i>
                    </a>
                </div>
            </div>

            <div class="col-sm-12 col-lg-3">
                <!-- time schedule -->
                <timeline></timeline>
            </div>

            <div class="col-sm-12 col-lg-3 order-lg-first">
                <!-- charts -->
                <chart></chart>
            </div>
        </div>
    </div>
    `,
    data() {
        return {
            days : [],
            dates : [],
            dateHighlight : {
                nowDate : new Date().getDate(),
                block : [1, 3],
                book : [8, 5, 10]
            }
            
        }
    },
    methods : {
        createCalendar() {
            // Get JSON
            axios.get('./data/calendar.json').then(data => {
                // pass days data to state
                this.days = data.data.days;
            })

            // set as async function
            let calendarview = async function() {
                
            }

            // get all varibles needed
            let date = new Date();
            let year = date.getFullYear();
            let month = date.getMonth();
            let startday = (new Date(year, month)).getDay();
            let startdate = 1;
            let lastMonth = new Date(year, month +1, 0).toDateString().split(' ');
            // start pushing dates by row
            for(let a = 0; a < 6; a++) {
                let tempArray = [];

                // add dates each column
                for(let b = 0; b < 7; b++) {
                    if(a == 0 && b < startday) {
                        tempArray.push(" ");
                    } else if(startdate > lastMonth[2]) {
                        break;
                    } else {
                        tempArray.push(startdate);
                        startdate++;
                    }
                }

                this.dates.push(tempArray);
            }
            //  set little delay
            setTimeout(() => {
                // set highlighted dates
                let tablebody = this.$el.querySelector('.main-calendar tbody').children;
                let getallrow = Array.from(tablebody);

                // loop and find the riht date
                getallrow.forEach((date, key) => {
                    let getallcol = Array.from(date.children);
                    getallcol.forEach((coldate) => {
                        // add currentdate class 
                        if(this.dateHighlight.nowDate == coldate.outerText) {
                            coldate.classList.add('currentdate');
                        }

                        // loop  booked
                        this.dateHighlight.book.forEach((book) => {
                            if(book == coldate.outerText) {
                                coldate.classList.add('bookeddate');
                            }
                        })
                        // llop for block

                        this.dateHighlight.block.forEach((block) => {
                            if(block == coldate.outerText) {
                                coldate.classList.add('blockdate');
                            }
                        })

                    });
                });
            }, 300);
            
        }
    },
    mounted() {
        // start creating calendar
        this.createCalendar();
        
    }
})